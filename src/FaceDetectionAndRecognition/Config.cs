﻿namespace FaceDetectionAndRecognition
{
    public static class Config
    {
        // set path to your destination directory
        public static string FacePhotosPath = "C:\\Users\\dimci\\Desktop\\Test\\Faces\\";
        public static string FaceListTextFile = "C:\\Users\\dimci\\Desktop\\Test\\FaceList.txt";
        public static string HaarCascadePath = "Resources\\haarcascade_frontalface_default.xml";
        public static int TimerResponseValue = 500;
        public static string ImageFileExtension = ".bmp";
        public static int ActiveCameraIndex = 0;
    }
}
